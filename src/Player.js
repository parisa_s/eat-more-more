var Player = cc.Sprite.extend({
	ctor: function(x,y) {
		this._super();
		this.initWithFile('res/images/player1Front.png');
		this.nextDirection = Player.DIR.STILL;
		this.direction = Player.DIR.STILL;

		this.score = 0;
		this.x = x;
		this.y = y;
		this.updatePosition();
	},

	update: function( dt ) {
		if(this.isAtCenter()){
			if(!this.isPosibleToMove( this.nextDirection )){
				this.nextDirection = Player.DIR.STILL;
			}
			this.direction = this.nextDirection;
		}
        switch ( this.direction ) {
        	case Player.DIR.UP:
           		this.y += Player.MOVE_STEP;
            	break;
        	case Player.DIR.DOWN:
            	this.y -= Player.MOVE_STEP;
            	break;
        	case Player.DIR.LEFT:
            	this.x -= Player.MOVE_STEP;
            	break;
        	case Player.DIR.RIGHT:
            	this.x += Player.MOVE_STEP;
            	break;
        }
        this.updatePosition();
    },

	updatePosition: function(){
		this.setPosition(new cc.Point(this.x, this.y));
	},

	setDirection: function( dir ){
		this.direction = dir;
	},

	setNextDirection: function( dir ){
		this.nextDirection = dir;
	},

	isAtCenter: function() {
		return ((this.x+20)%40==0)&&((this.y+20)%40==0);
	},

	setKitchen: function( kitchen ) {
		this.kitchen = kitchen;
	},

	isPosibleToMove: function( dir ){
		var nextBlockX = 0;
		var nextBlockY = 0; 
		if( dir == Player.DIR.STILL ){
			return true;
		}
		else if (dir == Player.DIR.UP ){
			nextBlockX = (this.x+20)/40-1;
			nextBlockY = (this.y+20+40)/40-1;
		}
		else if (dir == Player.DIR.RIGHT ){
			nextBlockX = (this.x+20+40)/40-1;
			nextBlockY = (this.y+20)/40-1;
		}
		else if (dir == Player.DIR.DOWN ){
			nextBlockX = (this.x+20)/40-1;
			nextBlockY = (this.y+20-40)/40-1;
		}
		else if (dir == Player.DIR.LEFT ){
			nextBlockX = (this.x+20-40)/40-1;
			nextBlockY = (this.y+20)/40-1;
		}
		return !this.kitchen.isWall( nextBlockX, nextBlockY );
	}
});

Player.MOVE_STEP = 5;
Player.DIR = {
	LEFT : 1, RIGHT : 2, UP : 3, DOWN : 4, STILL : 0 
};
Player.SPEED = 10;
Player.CHECK_MOVE = false;