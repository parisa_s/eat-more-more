var StartScene = cc.Scene.extend({
	onEnter: function() {
		this._super();
		this.bg = new cc.Sprite.create('res/images/BackgroundStart.png');
		//this.initWithFile('res/images/BackgroundStart.png');

		this.bg.setPosition(new cc.Point(400,300));
		this.addChild(this.bg);
		this.addBtnStart();
	},

	addBtnStart: function(){
		this.btnStart = new BtnStart();
		this.btnStart.setPosition(new cc.Point(400,270));
		this.btnStart.addBtnStartEvent(this);
		this.addChild( this.btnStart );

	}

});