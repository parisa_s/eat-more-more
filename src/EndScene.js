var EndScene = cc.Scene.extend({
	onEnter: function() {
		this._super();
		this.createScoreLabelP1();	
		this.createScoreLabelP2();
		this.showScore();
	},
	createScoreLabelP1: function() {
		this.scoreLabel1 = cc.LabelTTF.create('0','Arial',32);
		this.scoreLabel1.setPosition( cc.p(4*40,2*40+15));
		this.addChild( this.scoreLabel1 );
	},

	createScoreLabelP2: function() {
		this.scoreLabel2 = cc.LabelTTF.create('0','Arial',32);
		this.scoreLabel2.setPosition( cc.p(15*40,2*40+15));
		this.addChild( this.scoreLabel2 );
	},

	showScore: function(){
		this.scoreLabel1.setString(scoreP1.toString());
		this.scoreLabel2.setString(scoreP2.toString()); 
	}

});
