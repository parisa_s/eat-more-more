var GameLayer = cc.LayerColor.extend({
	init: function() {
		this._super( new cc.Color( 127, 127, 127, 255));
		this.setPosition( new cc.Point( 0, 0));

		this.createMap();
		this.addPlayer1();
		this.addPlayer2();
		this.addCake();
		this.createScoreLabelP1();
		this.createScoreLabelP2();
		this.createTimeLabelP1();
		this.createTimeLabelP2();
		this.addLifeP1();
		this.addLifeP2();

		this.date = new Date();
		this.startTime1 = this.date.getTime();
		this.currentTime1 = 0;

		this.date4 = new Date();
		this.startTime2 = this.date4.getTime();
		this.currentTime2 = 0;
		
		this.addKeyboardHanler();

		this.scheduleUpdate();

		this.player1.setKitchen( this.kitchen );
		this.player2.setKitchen( this.kitchen );
		return true;
	},

	createScoreLabelP1: function() {
		this.scoreLabel1 = cc.LabelTTF.create('0','Arial',32);
		this.scoreLabel1.setPosition( cc.p(4*40,2*40+15));
		this.addChild( this.scoreLabel1 );
	},

	createScoreLabelP2: function() {
		this.scoreLabel2 = cc.LabelTTF.create('0','Arial',32);
		this.scoreLabel2.setPosition( cc.p(15*40,2*40+15));
		this.addChild( this.scoreLabel2 );
	},

	createTimeLabelP1: function() {
		this.TimeLabel1 = cc.LabelTTF.create('10','Arial',32);
		this.TimeLabel1.setPosition( cc.p(7*40,2*40+15));
		this.addChild( this.TimeLabel1 );
	},

	createTimeLabelP2: function() {
		this.TimeLabel2 = cc.LabelTTF.create('10','Arial',32);
		this.TimeLabel2.setPosition( cc.p(13*40,2*40+15));
		this.addChild( this.TimeLabel2 );
	},

	createMap: function(){
		this.kitchen = new Kitchen();
		this.kitchen.setPosition( cc.p( 0, 40 ) );
        this.addChild( this.kitchen );
	},

	addPlayer1: function(){
		this.player1 = new Player1(1*40 + 20,3*40 + 20);
		this.kitchen.addChild(this.player1);
		this.player1.scheduleUpdate();
	},

	addPlayer2: function(){
		this.player2 = new Player2(18*40 + 20,3*40 + 20);
		this.kitchen.addChild(this.player2);
		this.player2.scheduleUpdate();
	},

	addCake: function(){
		var posiX ;
		var posiY ;
		while(true){
			posiX = Cake.randomPositionX();
			posiY = Cake.randomPositionY();

			if(!this.kitchen.isWall(posiX,posiY)){
				this.cake = new Cake((posiX*40)+20,(posiY*40)+20);
				console.log(posiX+" "+posiY);
				this.kitchen.addChild( this.cake );
				break;
			}
		}
		
	},

	addLifeP1: function(){
		this.lifeArr = [];
		var blockX = 4;
		var blockY = 1;
		for(var i=0 ; i<3 ; i++ ){
			this.lifeArr[i] = new Life();
			this.lifeArr[i].setPosition(new cc.Point(((blockX+i)*40)+20,(blockY*40)+20));
			console.log((((blockX+i)*40)+20)+" "+((blockY*40)+20));
			this.addChild( this.lifeArr[i] );
		}
		console.log("complete life P1");
	},

	addLifeP2: function(){
		this.lifeArr2 = [];
		var blockX = 16;
		var blockY = 1;
		console.log("XX");
		for(var i=0 ; i<3 ; i++ ){
			this.lifeArr2[i] = new Life();
			this.lifeArr2[i].setPosition(new cc.Point(((blockX+i)*40)+20,(blockY*40)+20));
			console.log((((blockX+i)*40)+20)+" "+((blockY*40)+20));
			this.addChild( this.lifeArr2[i] );
		}
		console.log("complete life P2");
	},

	updateScoreLabelP1: function(){
		this.scoreLabel1.setString( this.player1.score );
	},

	updateScoreLabelP2: function(){
		this.scoreLabel2.setString( this.player2.score );
	},

	updateTimeLabelP1: function(){
		this.TimeLabel1.setString( (parseInt(Math.ceil(((10000-(this.currentTime1-this.startTime1))/1000 ))).toString()));
	},

	updateTimeLabelP2: function(){
		this.TimeLabel2.setString( (parseInt(Math.ceil(((10000-(this.currentTime2-this.startTime2))/1000 ))).toString()));
	},

	resetTimeP1: function(){
		var date = new Date();
			this.startTime1 = date.getTime();
			this.currentTime1 = 0;
	},

	resetTimeP2: function(){
		var date = new Date();
			this.startTime2 = date.getTime();
			this.currentTime2 = 0;
	},

	update: function() {
		var date2 = new Date();
		var date3 = new Date();
		this.currentTime1 = date2.getTime();
		this.currentTime2 = date3.getTime();
		this.updateTimeLabelP1();
		this.updateTimeLabelP2();

		if(this.currentTime1 - this.startTime1>10000){
			this.removeChild( this.lifeArr[this.lifeArr.length-1]);
			this.lifeArr.splice(this.lifeArr.length-1);
			this.resetTimeP1();
		}
		else if(this.currentTime2 - this.startTime2>10000){
			this.removeChild( this.lifeArr2[this.lifeArr2.length-1]);
			this.lifeArr2.splice(this.lifeArr2.length-1);
			this.resetTimeP2();
		}
		this.collide(this.player1);
		this.collide(this.player2);
		this.isDead();
    },

    isDead: function(){
    	if(this.lifeArr == 0||this.lifeArr2 == 0){
    		scoreP1 = this.player1.score;
    		scoreP2 = this.player2.score;
    		cc.director.runScene(new EndScene());
    	}
    },

    collide : function(player) {
    	if(player == this.player1){
			if ( this.cake.checkCollision( this.player1 ) ) {
				this.player1.score++;
				this.updateScoreLabelP1();
				this.resetTimeP1();

				var posiX ;
			var posiY;
			while(true){
				//console.log(this.player.score);
				posiX = Cake.randomPositionX();
				posiY = Cake.randomPositionY();

				if(!this.kitchen.isWall(posiX,posiY)&&posiY>=2&&posiY<=13){
					this.cake.setPosition((posiX*40)+20,(posiY*40)+20);
					console.log(posiX+" "+posiY);
					break;
				} 
			}
			}
		}
		else {
			if(this.cake.checkCollision(this.player2)){
				this.player2.score++;
				this.updateScoreLabelP2();
				this.resetTimeP2();

				var posiX ;
			var posiY;
			while(true){
				//console.log(this.player.score);
				posiX = Cake.randomPositionX();
				posiY = Cake.randomPositionY();

				if(!this.kitchen.isWall(posiX,posiY)&&posiY>=2&&posiY<=13){
					this.cake.setPosition((posiX*40)+20,(posiY*40)+20);
					console.log(posiX+" "+posiY);
					break;
				} 
			}
			}
		}	
			
		},

	onKeyDown: function( keyCode, event ) {
        switch( keyCode ) {
        	case cc.KEY.left:
            	this.player1.setNextDirection( Player.DIR.LEFT );
            	break;
        	case cc.KEY.right:
            	this.player1.setNextDirection( Player.DIR.RIGHT );
            	break;
        	case cc.KEY.up:
            	this.player1.setNextDirection( Player.DIR.UP );
            	break;
        	case cc.KEY.down:
            	this.player1.setNextDirection( Player.DIR.DOWN );
            	break;
            case cc.KEY.w :
        		this.player2.setNextDirection( Player.DIR.UP );
        		break;
        	case cc.KEY.a:
            	this.player2.setNextDirection( Player.DIR.LEFT );
            	break;
        	case cc.KEY.s:
            	this.player2.setNextDirection( Player.DIR.DOWN );
            	break;
        	case cc.KEY.d:
            	this.player2.setNextDirection( Player.DIR.RIGHT);
            	break;
        	
        }
    },

	onKeyUp: function( keyCode, event ) {
		switch( keyCode){
			case cc.KEY.left: 
			case cc.KEY.right: 
			case cc.KEY.up: 
			case cc.KEY.down:
				this.player1.setNextDirection( Player.DIR.STILL);
				break;
			case cc.KEY.w: 
			case cc.KEY.a: 
			case cc.KEY.s: 
			case cc.KEY.d:
				this.player2.setNextDirection( Player.DIR.STILL);
				break;
		}
	},

	addKeyboardHanler: function() {
		var self = this;
		cc.eventManager.addListener({
			event: cc.EventListener.KEYBOARD,
			onKeyPressed : function ( keyCode, event ) {
				self.onKeyDown( keyCode, event );
			},
			onKeyReleased: function ( keyCode, event ) {
				self.onKeyUp( keyCode, event );
			}
		},this);
	}
});

