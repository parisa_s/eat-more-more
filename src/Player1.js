var Player1 = Player.extend({
	ctor: function(x,y){
		this._super();
		this.initWithFile('res/images/player1Front.png');


		this.x = x;
		this.y = y;
		this.updatePosition();

	},
	update: function( dt ) {
		if(this.isAtCenter()){
			if(!this.isPosibleToMove( this.nextDirection )){
				this.nextDirection = Player.DIR.STILL;
			}
			this.direction = this.nextDirection;
		}
        switch ( this.direction ) {
        	case Player.DIR.UP:
           		this.y += Player.MOVE_STEP;
            	break;
        	case Player.DIR.DOWN:
            	this.y -= Player.MOVE_STEP;
            	break;
        	case Player.DIR.LEFT:
            	this.x -= Player.MOVE_STEP;
            	break;
        	case Player.DIR.RIGHT:
            	this.x += Player.MOVE_STEP;
            	break;
        }
        this.updatePosition();
    },

    onKeyDown: function( keyCode, event ) {
        switch( keyCode ) {
        	case cc.KEY.left:
            	this.setNextDirection( Player.DIR.LEFT );
            	break;
        	case cc.KEY.right:
            	this.setNextDirection( Player.DIR.RIGHT );
            	break;
        	case cc.KEY.up:
            	this.setNextDirection( Player.DIR.UP );
            	break;
        	case cc.KEY.down:
            	this.setNextDirection( Player.DIR.DOWN );
            	break;
        }
    },

	onKeyUp: function( keyCode, event ) {
		this.setNextDirection( Player.DIR.STILL);
	},

	addKeyboardHanler: function() {
		var self = this;
		cc.eventManager.addListener({
			event: cc.EventListener.KEYBOARD,
			onKeyPressed : function ( keyCode, event ) {
				self.onKeyDown( keyCode, event );
			},
			onKeyReleased: function ( keyCode, event ) {
				self.onKeyUp( keyCode, event );
			}
		},this);
	},

	isPosibleToMove: function( dir ){
		var nextBlockX = 0;
		var nextBlockY = 0; 
		if( dir == Player.DIR.STILL ){
			return true;
		}
		else if (dir == Player.DIR.UP ){
			nextBlockX = (this.x+20)/40-1;
			nextBlockY = (this.y+20+40)/40-1;
		}
		else if (dir == Player.DIR.RIGHT ){
			nextBlockX = (this.x+20+40)/40-1;
			nextBlockY = (this.y+20)/40-1;
		}
		else if (dir == Player.DIR.DOWN ){
			nextBlockX = (this.x+20)/40-1;
			nextBlockY = (this.y+20-40)/40-1;
		}
		else if (dir == Player.DIR.LEFT ){
			nextBlockX = (this.x+20-40)/40-1;
			nextBlockY = (this.y+20)/40-1;
		}
		return !this.kitchen.isWall( nextBlockX, nextBlockY );
	}
});
Player.DIR = {
	LEFT : 1, RIGHT : 2, UP : 3, DOWN : 4, STILL : 0 
};