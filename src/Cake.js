var Cake = cc.Sprite.extend({
	ctor: function(x,y) {
		this._super();
		this.initWithFile('res/images/cake.png');

		this.x = x;
		this.y = y;
		this.updatePosition();
	},

	updatePosition: function(){
		this.setPosition(new cc.Point(this.x, this.y));
	},
	
	checkCollision: function( obj ){
		var myPos = this.getPosition();
		var objPos = obj.getPosition();
		return ((Math.abs(myPos.x - objPos.x) <= DISTANCE_OBJ)&& (Math.abs(myPos.y - objPos.y) <= DISTANCE_OBJ));
	}
});

	Cake.randomPositionX = function(){
		return parseInt(Math.floor(Math.random()*20));
	},

	Cake.randomPositionY =  function() {
		// return Math.floor(((Math.random()*600)+20)/40-1);
		return parseInt(Math.floor(Math.random()*15));
	}

DISTANCE_OBJ = 16;