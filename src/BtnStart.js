var BtnStart = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/btStart.png');
	},
	addBtnStartEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 250 && event.getLocationX() < 550 
					&& event.getLocationY() >   220 && event.getLocationY() < 320) {
						 cc.director.runScene(new GameScene());
				}
			},
			onMouseMove: function(event) {
				if(event.getLocationX() > 250 && event.getLocationX() < 550 
					&& event.getLocationY() >   220 && event.getLocationY() < 320) {
						
						console.log('change');				}
			}
		}, this);
	}
});

